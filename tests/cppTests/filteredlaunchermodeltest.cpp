#include <QtTest/QtTest>
#include "filteredlaunchermodeltest.h"
#include "filteredlaunchermodel.h"
#include "fakelaunchermodel.h"
#include "launcheritemmock.h"

class FilteredLauncherModelTest : public QObject
{
    Q_OBJECT
public:
    explicit FilteredLauncherModelTest(QObject *parent = 0);
    virtual ~FilteredLauncherModelTest();

signals:

public slots:

private slots:
    void canFilterByTitle();

private:
    // Returns fresh object list model that looks like a set of real launcher items
    QObjectListModel* prepareSampleLauncherModel();

    QList<LauncherItemMock*> m_sampleItems;
};

FilteredLauncherModelTest::FilteredLauncherModelTest(QObject *parent) :
    QObject(parent)
{
    m_sampleItems.append(new LauncherItemMock("Flashlight"));
    m_sampleItems.append(new LauncherItemMock("HelloWorld Pro"));
    m_sampleItems.append(new LauncherItemMock("Gallery"));
    m_sampleItems.append(new LauncherItemMock("Flashlight Pro"));
    m_sampleItems.append(new LauncherItemMock("Flowers"));
    m_sampleItems.append(new LauncherItemMock("Get Flowers"));
}

FilteredLauncherModelTest::~FilteredLauncherModelTest()
{
    qDeleteAll(m_sampleItems);
    m_sampleItems.clear();
}

void FilteredLauncherModelTest::canFilterByTitle()
{
    QScopedPointer<QObjectListModel> sampleLauncherModel(prepareSampleLauncherModel());
    FilteredLauncherModel flm(sampleLauncherModel.data());

    QCOMPARE(sampleLauncherModel->rowCount(), 6);
    QCOMPARE(flm.rowCount(), 6);

    flm.setFilterFixedString("fl");
    QCOMPARE(flm.rowCount(), 4);
}

QObjectListModel* FilteredLauncherModelTest::prepareSampleLauncherModel()
{
    FakeLauncherModel* result = new FakeLauncherModel();
    QList<LauncherItemMock*>::iterator i;
    for (i = m_sampleItems.begin(); i != m_sampleItems.end(); i++) {
        result->addItem(*i);
    }

    return result;
}


QTEST_MAIN(FilteredLauncherModelTest)
#include "filteredlaunchermodeltest.moc"
