#ifndef FAKELAUNCHERMODEL_H
#define FAKELAUNCHERMODEL_H

#include "qobjectlistmodel.h"

class FakeLauncherModel : public QObjectListModel
{
    Q_OBJECT
public:
    explicit FakeLauncherModel(QObject *parent = 0);

signals:

public slots:

};

#endif // FAKELAUNCHERMODEL_H
