#ifndef LAUNCHERITEMMOCK_H
#define LAUNCHERITEMMOCK_H

#include <QObject>

class LauncherItemMock : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString title READ title NOTIFY itemChanged)
    Q_PROPERTY(bool isValid READ isValid NOTIFY itemChanged)

public:
    explicit LauncherItemMock(const QString& title, QObject *parent = 0);

QString title() const
{
    return m_title;
}

bool isValid() const
{
    return m_isValid;
}

signals:

void itemChanged(QString arg);

public slots:

private:
    QString m_title;
    bool m_isValid;
};

#endif // LAUNCHERITEMMOCK_H
