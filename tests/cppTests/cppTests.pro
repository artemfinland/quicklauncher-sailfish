TEMPLATE = app

# The name of your app
TARGET = tst-harbour-quicklauncher-cpp

QT += testlib

CONFIG += testcase

TARGETPATH = /usr/bin
target.path = $$TARGETPATH

DEPLOYMENT_PATH = /usr/share/$$TARGET
qml.path = $$DEPLOYMENT_PATH

INCLUDEPATH += ../../src/app

LIBS += -L$$OUT_PWD/../../src/lipstick-launchermonitor/ -llipstick-launchermonitor
INCLUDEPATH += $$PWD/../../src/lipstick-launchermonitor/src/lipstick-components
LIBS += -L$$OUT_PWD/../../src/engine/ -lharbour-quicklauncher-engine
INCLUDEPATH += $$PWD/../../src/engine
QMAKE_RPATHDIR += /usr/share/harbour-quicklauncher/lib

# C++ sources
SOURCES += \
    filteredlaunchermodeltest.cpp \
    launcheritemmock.cpp \
    fakelaunchermodel.cpp

# C++ headers
HEADERS += \
#    filteredlaunchermodeltest.h \
    launcheritemmock.h \
    fakelaunchermodel.h \
    $$PWD/../../src/engine/filteredlaunchermodel.h

INSTALLS += target

