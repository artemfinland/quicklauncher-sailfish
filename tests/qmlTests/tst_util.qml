import QtQuick 2.0
//import "../../src/app/qml/js/util.js" as Util
import QtTest 1.0
import "../harbour-quicklauncher/qml/js/util.js" as Util

TestCase {
    name: "util.js tests"

    QtObject {
        id: fakeLauncherItem
        property string m_iconFilename: ""
        function iconFilename() {
            return m_iconFilename
        }

        property string iconId: ""
    }

    function init() {
        fakeLauncherItem.m_iconFilename = ""
        fakeLauncherItem.iconId = ""
    }

    function test_passesIconFileNameWhenSpecified() {
        fakeLauncherItem.m_iconFilename = "/usr/share/definedFilename"
        var calcedFilename = Util.iconPathFromLauncherItem(fakeLauncherItem)
        compare(calcedFilename, "/usr/share/definedFilename", "Failed to pass iconFilename when launcher item had it")
    }

    function test_returnsThemePathWhenIconFileNameisNotSpecified() {
        fakeLauncherItem.iconId = "icon-launcher-default"
        var calcedFilename = Util.iconPathFromLauncherItem(fakeLauncherItem)
        compare(calcedFilename, "image://theme/icon-launcher-default", "Failed to generate a proper theme path")
    }

    function test_figuredOutCorrectIconForAndroidApps() {
        fakeLauncherItem.iconId = "/var/lib/apkd/apkd_launcher_com_accuweather_android-com_accuweather_android_LauncherActivity.png"
        var calcedFilename = Util.iconPathFromLauncherItem(fakeLauncherItem)
        compare(calcedFilename, "/var/lib/apkd/apkd_launcher_com_accuweather_android-com_accuweather_android_LauncherActivity.png", "Failed to generate a proper theme path")
    }
}
