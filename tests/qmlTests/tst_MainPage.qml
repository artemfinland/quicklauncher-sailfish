/**
 * Testing on the UI level when nothing lower level is invented
 */
import QtQuick 2.0
import QtTest 1.0
import Sailfish.Silica 1.0

//import "../src/qml/pages"
import "../harbour-quicklauncher/qml/pages"


// Putting TestCase into the full app structure to test UI interactions and probably page transitions too
ApplicationWindow {
    id: wholeApp

    Component {
        id: mainPageComponent
        MainPage {
            id: mainPage
            launcherModel: fakeModel
        }
    }

    ListModel {
        id: fakeModel
        ListElement { object {title: "Flowers app"}}
        ListElement { object {title: "Good floor"}}
        ListElement { object {title: "Hello world"}}
        ListElement { object {title: "Hello world Pro"}}
    }


    TestCase {
        name: "mainPage tests"
        when: windowShown

        property var mainPage: null

        function init() {
            mainPage = mainPageComponent.createObject(null)
        }

        function cleanup() {
            mainPage.destroy()
            mainPage = null
        }

        function test_showsAllItemsInitially() {
            compare(mainPage._i.launcherList.count, 4, "List view should contain four supplied items")
            compare(mainPage._i.launcherList.currentIndex, 0, "list should be focused on the top element initially")
        }

    }
}
