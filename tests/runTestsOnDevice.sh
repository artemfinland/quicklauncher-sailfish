#!/bin/bash

# Script for running QML tests. That's for specifying just one argument in QtCreator's configuration
/usr/bin/tst-harbour-quicklauncher-cpp
/usr/bin/tst-harbour-quicklauncher-qml -input /usr/share/tst-harbour-quicklauncher-qml

# When you'll get some QML components in the main app, you'll need to import them to the test run
# /usr/bin/tst-harbour-quicklauncher -input /usr/share/tst-harbour-quicklauncher -import /usr/share/harbour-quicklauncher/qml/components
