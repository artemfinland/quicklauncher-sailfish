import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.quicklauncher 0.1
import "pages"

ApplicationWindow
{
    id: app
    readonly property string _APP_VERSION: appVersion
    readonly property string _APP_BUILD_NUMBER: appBuildNum

    LauncherModel {
        id: deviceLauncherModel
    }

    initialPage: MainPage {
        launcherModel: FilteredLauncherModel {
            sourceModel: deviceLauncherModel
        }
    }

    cover: Qt.resolvedUrl("cover/CoverPage.qml")

}


