import QtQuick 2.0
import Sailfish.Silica 1.0

Page {


    PageHeader {
        id: pageHeader
        title: "About"
    }

    Label {
        id: aboutLabel
        anchors.top: pageHeader.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: Theme.paddingLarge

        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Text.AlignHCenter
        text: "Quick Launcher " + app._APP_VERSION + " (" +app._APP_BUILD_NUMBER +
              ")<br/>" +
              "by Artem Marchenko<br/>" +
//              "and Ekaterina Kuts<br/>
              "<br/>" +
              "Enjoy!<br/><br/>"
    }

    Label {
        id: extrnalLibsNote
        anchors.top: aboutLabel.bottom
//        anchors.topMargin: Theme.paddingMedium
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: Theme.paddingLarge

        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Text.AlignHCenter
        textFormat: Text.RichText
        text: "\
        <style>
            li {
                margin-bottom: 15px;
            }
            a {
                color: " +Theme.highlightColor +";\
            }
        </style>
        This app used lipstick-launchermonitor library published under GPL 2.1 \
              You can download source code at <a href='https://github.com/amarchen/lipstick-launchermonitor'>Artem's github</a>. \
              lipstick-launchermonitor license (just GPL 2.1 actually) can be read <a href='https://github.com/amarchen/lipstick-launchermonitor/blob/master/LICENSE'>here</a>."



        onLinkActivated: {
            Qt.openUrlExternally(link)
//            pageStack.push("BrowserPage.qml", {"url": link})
        }

    }

//    Button {
//        anchors.top: aboutLabel.bottom
//        anchors.topMargin: Theme.paddingMedium
//        anchors.horizontalCenter: aboutLabel.horizontalCenter
//        text: "See changelog"
//        onClicked: pageStack.push("ChangeLogPage.qml")
//    }

}
