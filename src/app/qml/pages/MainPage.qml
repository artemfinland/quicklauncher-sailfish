import QtQuick 2.0
import Sailfish.Silica 1.0
import "../js/util.js" as Util

Page {
    property alias launcherModel: launcherList.model

    // for testability
    property var _i: QtObject {
        property alias launcherList: launcherList
        property alias searchField: searchField
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: "Privacy"
                onClicked: {
                    pageStack.push("PrivacyPage.qml")
                }
            }
            MenuItem {
                text: "About"
                onClicked: {
                    pageStack.push("AboutPage.qml")
                }
            }
        }


    SearchField {
        id: searchField
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        focus: true

        EnterKey.enabled: launcherList.currentIndex > -1
        EnterKey.iconSource: "image://theme/icon-m-enter-accept"

        EnterKey.onClicked: {
            console.log("sEntered")
            launcherList.currentItem.clicked(null)
        }

        onTextChanged: {
            launcherModel.setFilterFixedString(text)
        }

    }

    SilicaListView {
        id: launcherList
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: searchField.bottom
        anchors.bottom: parent.bottom
        clip: true

        focus: true

        delegate: BackgroundItem {
            id: launcherItemLine
            height: Theme.itemSizeMedium
            highlighted: ListView.isCurrentItem

            Image {
                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingLarge
                anchors.verticalCenter: parent.verticalCenter
                width: 86  // primarily for resizing differently sized Android icons to Sailfish standard size
                height: 86

                id: appIcon
                source: Util.iconPathFromLauncherItem(object)
            }
            Label {
                id: label
                anchors.left: appIcon.right
                anchors.leftMargin: Theme.paddingMedium
                anchors.verticalCenter: appIcon.verticalCenter
                height: parent.height

                anchors.right: parent.right

                text: object.title
                color: ListView.highlighted ? Theme.highlightColor : Theme.primaryColor
            }

            onDownChanged: {
                if(down) {
                    launcherList.currentIndex = index
                }
            }

            onClicked: {
                object.launchApplication()
                // And no need for launcher to keep a task manager slot then
                Qt.quit()
            }
        }
    }
    }


}

