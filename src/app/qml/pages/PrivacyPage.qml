import QtQuick 2.0

import Sailfish.Silica 1.0

Page {
    PageHeader {
        id: header
        title: "Privacy"
    }

    SilicaFlickable {
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: Theme.paddingLarge

        contentHeight: privacyNote.height

        Label {
            id: privacyNote
            anchors.centerIn: parent
            width: parent.width
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            horizontalAlignment: Text.AlignHCenter
            textFormat: Text.RichText
            text: "\
<style>
    li {
        margin-bottom: 15px;
    }
    a {
        color: " +Theme.highlightColor +";\
    }
</style>
                    <h2>Short version</h2> \
<ul>
    <li>
        Quick Launcher app doesn't collect any data about you
    </li>\
    <li>\
        In the future I will probably add some settings to let you share with us whether app is used often and how many seconds it saves for you if any\
    </li>\
    <li>\
        That is for my motivation, AB-testing improvement ideas and possibly for fun stats for you \
    </li>\
</ul>\
                    <h2>Long version</h2>\
<ul>
    <li>
        I am motivated by the fact that app is actually used. Also I want to know how much this or that improvement is actually useful. There are a couple of optimization ideas that may or may not speed\
 things up depending on how many apps you have. It would help AB-testing these if app could run an idea on random 10% of users and see if it helps on average.
    </li>\
    <li>\
        If I ever get to collecting stats, app will introduce some clear setting for you to opt in or out then
    </li>\
    <li>\
        If you want to help both developers see what happens with their apps and users to have their privacy protected,
        <a href='https://together.jolla.com/question/10956'>go vote for Jolla to create a centralized user controllable trusted stats gathering</a>
    </li>\
</ul>
                    "

            onLinkActivated: {
                Qt.openUrlExternally(link)
    //            pageStack.push("BrowserPage.qml", {"url": link})
            }
        }
    }

}
