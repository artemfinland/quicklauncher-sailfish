.pragma library

/**
 * Returns an icon path ready for passing to Image element from LauncherItem passed
 * I.e. it's either /usr/share/icons... or image://theme/icons/
 */
function iconPathFromLauncherItem(launcherItem) {
    // We don't have reliable mechanism for differentiating between theme icons and android icons
    // So speculating that icon that includes "/" is an android icon and image://theme should not be prepended then
    var iconHasPathComponent = launcherItem.iconId.indexOf("/") !== -1;
    if(iconHasPathComponent) {
        return launcherItem.iconId
    }

    // if iconFilename specified, then let's return it
    // otherwise assume some standard theme icon specified in iconId
    return launcherItem.iconFilename() || "image://theme/" + launcherItem.iconId
}
