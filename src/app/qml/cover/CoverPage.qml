import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    Image {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: Theme.paddingLarge*2
        anchors.verticalCenter: parent.verticalCenter
        height: width

        // Own app icon :)
        source: "/usr/share/icons/hicolor/86x86/apps/harbour-quicklauncher.png"
    }
}
