#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>
#include <QScopedPointer>
#include <QQuickView>
#include <QQmlEngine>
#include <QGuiApplication>

#include "filteredlaunchermodel.h"
#include "launchermodel.h"

int main(int argc, char *argv[])
{

    qmlRegisterUncreatableType<QAbstractItemModel>("harbour.quicklauncher", 0, 1, "AbstractItemModel", "Abstract item model cannot be created, it's abstract");
    qmlRegisterType<FilteredLauncherModel>("harbour.quicklauncher", 0, 1, "FilteredLauncherModel");
    qmlRegisterType<LauncherModel>("harbour.quicklauncher", 0, 1, "LauncherModel");

    QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
    QScopedPointer<QQuickView> view(SailfishApp::createView());
    view->rootContext()->setContextProperty("appVersion", APP_VERSION);
    view->rootContext()->setContextProperty("appBuildNum", APP_BUILDNUM);
    QObject::connect((QObject*)view->engine(), SIGNAL(quit()), app.data(), SLOT(quit()));

    view->setSource(SailfishApp::pathTo("qml/main.qml"));

    view->show();

    return app->exec();
}
