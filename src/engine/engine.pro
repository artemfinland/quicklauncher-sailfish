TEMPLATE=lib
# The name of your app binary (and it's better if you think it is the whole app name as it's referred to many times)
# Must start with "harbour-"
TARGET = harbour-quicklauncher-engine
TARGETPATH = /usr/share/harbour-quicklauncher/lib
target.path = $$TARGETPATH

LIBS += -L$$OUT_PWD/../lipstick-launchermonitor/ -llipstick-launchermonitor
INCLUDEPATH += $$PWD/../lipstick-launchermonitor/src/lipstick-components
QMAKE_RPATHDIR += /usr/share/harbour-quicklauncher/lib

INSTALLS += target

SOURCES += \
    filteredlaunchermodel.cpp

INCLUDEPATH += $$PWD

HEADERS += \
    filteredlaunchermodel.h
