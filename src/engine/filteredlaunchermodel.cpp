#include "filteredlaunchermodel.h"

#include "qobjectlistmodel.h"
#include "launcheritem.h"
#include <QDebug>

FilteredLauncherModel::FilteredLauncherModel(QObjectListModel *sourceModel, QObject *parent) :
    QSortFilterProxyModel(parent)
{
    // TODO: put general constructor stuff to a separate function
    setSourceModel(sourceModel);
    init();
//    setSortingEnabled(true);
}

FilteredLauncherModel::FilteredLauncherModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
    init();
}

void FilteredLauncherModel::init()
{
    setFilterRole(Qt::UserRole + 1); // hard coded in QObjectListModel
    setSortRole(Qt::UserRole + 1); // hard coded in QObjectListModel
    setFilterCaseSensitivity(Qt::CaseInsensitive);
    setDynamicSortFilter(true);
    sort(0);
}



bool FilteredLauncherModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex mi = qobject_cast<QObjectListModel*>(sourceModel())->index(source_row, 0, source_parent);

    const QVariant& vLaunchItem = qobject_cast<QObjectListModel*>(sourceModel())->data(mi, Qt::UserRole + 1);
    const QObject* oLi = qvariant_cast<QObject*>(vLaunchItem);

    return (!oLi->property("isValid").toBool()) || oLi->property("title").toString().contains(filterRegExp());
}

bool FilteredLauncherModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    const QVariant& vLeftItem = qobject_cast<QObjectListModel*>(sourceModel())->data(left, Qt::UserRole + 1);
    const QVariant& vRightItem = qobject_cast<QObjectListModel*>(sourceModel())->data(right, Qt::UserRole + 1);
    const QObject* oLeftItem = qvariant_cast<QObject*>(vLeftItem);
    const QObject* oRightItem = qvariant_cast<QObject*>(vRightItem);
    const QString& leftTitle = oLeftItem->property("title").toString();
    const QString& rightTitle = oRightItem->property("title").toString();
//    qDebug() << "l: " << leftTitle << "; r: " << rightTitle;
    return leftTitle.localeAwareCompare(rightTitle) < 0;
}
