#ifndef FILTEREDLAUNCHERMODEL_H
#define FILTEREDLAUNCHERMODEL_H

#include <QObject>

#include <QSortFilterProxyModel>

class QObjectListModel;

class FilteredLauncherModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    /** sourceModel should look like LauncherModel. I.e. have items with title() */
    explicit FilteredLauncherModel(QObjectListModel* sourceModel, QObject *parent = 0);
    explicit FilteredLauncherModel(QObject *parent = 0);

protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    virtual bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

signals:

public slots:

private:
    // Initialization common for both constructors
    void init();

};

#endif // FILTEREDLAUNCHERMODEL_H
